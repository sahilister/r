# Emacs APAC redirected links

This repository is used to provide few short URLs at
[https://emacs-apac.gitlab.io/r/*](https://emacs-apac.gitlab.io/r/).
